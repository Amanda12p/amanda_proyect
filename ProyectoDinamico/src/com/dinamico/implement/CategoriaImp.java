package com.dinamico.implement;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Repository;

import com.dinamico.utils.AbsFacade;
import com.dinamico.utils.Dao;
import com.dinamico.model.Categoria;;

@Repository
public class CategoriaImp extends AbsFacade<Categoria> implements Dao<Categoria>{

	@Autowired
	private SessionFactory session;
	
	@Autowired
	private SessionFactory getSeFactory;
	
	public CategoriaImp() {
		super(Categoria.class);
		// TODO Auto-generated constructor stub
	}

	@Override
	public SessionFactory getSessionFactory() {
		// TODO Auto-generated method stub
		return session;
	}

}
