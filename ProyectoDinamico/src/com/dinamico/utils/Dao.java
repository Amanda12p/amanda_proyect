package com.dinamico.utils;

import java.util.List;

public interface Dao<T> {
    public void create(T t);
    public List<T> findAll();
    public T find(Object id);
    public void update(T t);
    public void delete(T t);
}
