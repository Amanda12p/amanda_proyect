package com.dinamico.utils;

import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan("com.dinamico")
public class Configuracion {

	@Bean
	public SessionFactory getSeFactory() {		
		return Hib.getSessionFactory();
	}
		
}
