package com.dinamico.utils;

import java.util.List;

import org.hibernate.SessionFactory;

/**
 * @author Elena
 */
public abstract class AbsFacade<T> {

    private Class<T> entityClass;

    public AbsFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    public abstract SessionFactory getSessionFactory();


    public void create(T entity) {
        getSessionFactory().getCurrentSession().save(entity);
    }

    public void update(T entity) {
        getSessionFactory().getCurrentSession().update(entity);
    }

    public void delete(T entity) {
        getSessionFactory().getCurrentSession().delete(entity);
    }

    public T find(Object id) {
        return getSessionFactory().getCurrentSession().find(entityClass, id);
    }

    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getSessionFactory().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getSessionFactory().getCurrentSession().createQuery(cq).getResultList();
    }

}
